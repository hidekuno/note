---
title: "Git"
date: 2019-01-30T13:28:16+09:00
archives: "2019"
tags: [develop]
author: hidekuno
---

グローバルな設定
```
git config --global user.name "hidekuno"
git config --global user.email "hidekuno@gmail.com"
git config --global core.autocrlf=false
git config --global color.ui "auto"
git config --global core.editor vim
```

リモートの内容を取り込む 強制的(オーバーライト)
```
git reset --hard origin/master
```

直前のコミットを取り消す
```
git reset --hard HEAD^
git push -f origin HEAD^:develop
```

リモートより特定のファイルを取り出す
```
git checkout remotes/origin/develop tool/README
```

tag関連
```
git tag -l -n
git tag -a v0.3 -m 'my version 0.3'
git tag -d v0.3
git push origin :v0.3
git tag -a v0.3 -m 'test code fix'
git push origin v0.3
git push origin :v0.2
git push origin :v0.3
git tag v0.2 f6f2358 -m 'Release drawing tool'
git tag v0.21 80caa9a -m 'Release tail recursive'
git push origin --tags
```

commit logを綺麗にする
```
git rebase -i HEAD~3
+----------------------------------------------------------------------+
| pick ec1b559 dovecot停止処理エラーに対応した(retryを3回行う)         | 
| pick 332f3af typo                                                    | 
| pick bc89b55 typo                                                    |
+----------------------------------------------------------------------+

+----------------------------------------------------------------------+
| pick ec1b559 dovecot停止処理エラーに対応した(retryを3回行う)         |
| s    332f3af typo                                                    | 
| s    bc89b55 typo                                                    | 
+----------------------------------------------------------------------+
```

リモートサーバとブランチの同期をとる。
```
git fetch --all
git reset --hard origin/feature/set-contents-plugin
git pull origin feature/set-contents-plugin
```

コミットメッセージを変更する
```
git commit --amend -m "#212 repair用Chef ログローテート版 新規公開"
```

削除したファイルの復活を行う
```
git reset $directory
git checkout HEAD $filename
```

git addを取り消す
```
git reset .
git checkout .
```

commitIDから特定のファイルを取り出す(例)
```
git checkout 5b7e3de12674321554a0d56f80437feb77941bd1 Cargo.toml
```
コミットはせずに変更を退避して,別ブランチに繁栄
```
git stash save
git stash list
git checkout master
git checkout -b bug_time_measurement
git stash apply stash@{0}
git add -u
git stash drop stash@{0}
```

外部の git リポジトリを、自分の git リポジトリのサブディレクトリとして登録
```
git submodule add https://github.com/Tazeg/hugo-blog-jeffprod.git themes/jeffprod
git submodule init
git submodule update
```

git flowの導入
```
git clone https://github.com/nvie/gitflow.git
git submodule init
git submodule update
```

過去のauthor, commiterを変更する
```
git log --pretty=full

git filter-branch -f --commit-filter '
  if [ "$GIT_COMMITTER_EMAIL" = "kunohi@firstserver.co.jp" ];
    then
        GIT_COMMITTER_NAME="hidekuno";
        GIT_AUTHOR_NAME="hidekuno";
        GIT_COMMITTER_EMAIL="hidekuno@gmail.com";
        GIT_AUTHOR_EMAIL="hidekuno@gmail.com";
        git commit-tree "$@";
    else
        git commit-tree "$@";
    fi'  HEAD
```

git logの一番見易いやり方
```
git log --oneline --decorate --tags
```

日本語ファイル名の文字化けを防止する
```
git config --local core.quotepath false
```

git リポジトリの作成(例)
```
git init --bare --shared
git init --shared
git init
```

proxy環境の対応
```
git config --global http.proxy http://192.168.127.1:8080
git config --global https.proxy http://192.168.127.1:8080
```

proxy環境の対応(socks5)
```
git config --global http.proxy socks5://localhost:1080
git config --global https.proxy socks5://localhost:1080
```

特定のブランチよりファイルを表示する
```
git show vect:glisp/src/coord.rs
```

特定のブランチよりファイルをコピーする
```
git checkout vect glisp/src/coord.rs
```

error: object file .git/objects/${where}/${where} is emptyが発生した場合は下コマンドでチェックする
```
git fsck --full
```

日本語ファイル名が化けた時の対処
```
 git config --global core.quotepath false
```

リモートリポジトリのデフォルトブランチ名変更
```
git remote set-head origin develop
```

リモートのブランチを削除する
```
git push origin :test-cicd
```
```
git push origin --delete test-cicd
```

リモートリポジトリのmainを取得し、ローカルブランチmainの上流ブランチに設定する
```
git remote set-branches origin main
git fetch --set-upstream origin main
```

リモートリポジトリのデフォルトブランチをmainに変更する
```
git remote set-head -a origin
```

特定のコミットを取り消す
```
git revert 8763312
```