---
title: "WSLについて記録"
date: 2020-04-07T15:20:49+09:00
archives: "2020"
tags: [os]
author: "hidekuno"
---

1. https://docs.microsoft.com/ja-jp/sysinternals/downloads/ctrl2capよりctrlとcapsを入れ替える
1. WSL(Windows Subsystem for Linux) を有効にする
1. Windows Storeよりubuntuをインストールする
1. dockerをインストールする
    1. 現場がproxyを利用しているのでdockerのconfigで設定する
1. VcXsrvをインストールする
1. emacsをインストールする
    1. 現場がproxyを利用しているのでaptの下記内容の設定を行う
cat  /etc/apt/apt.conf
```
Acquire::http::Proxy "http://192.168.127.1:8080";
Acquire::https::Proxy "http://192.168.127.1:8080";
```
sudoの設定

sudo apt-get remove nano

visudo
```
hideki ALL=NOPASSWD: ALL
```

pythonの設定(python3を優先とする)
```
sudo update-alternatives --install /usr/bin/python python /usr/bin/python3.8 1
sudo update-alternatives --install /usr/bin/pip pip /usr/bin/pip3 1
```

wslでの記録

```
sudo apt-get install apt-transport-https ca-certificates curl software-properties-common
curl -x http://192.168.33.4:8080 -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu  $(lsb_release -cs) stable"
sudo apt-get install -y docker-ce
sudo usermod -aG docker hidekuno
sudo apt-get install python3-pip
sudo apt-get install python3-pandas
sudo apt-get install jupyter-notebook

jupyter-notebook
git config --global http.proxy http://192.168.127.1:8080
git config --global https.proxy http://192.168.127.1:8080

sudo mkdir /mnt/share
sudo mount -t drvfs '\\osaka-fs\Project\' /mnt/share
sudo apt -y install language-pack-ja
sudo update-locale LANG=ja_JP.UTF8

sudo usermod -aG sudo hideki
sudo apt-get install libssl-dev
sudo apt-get install npm
sudo apt-get install openjdk-11-jdk
sudo apt-get install openjdk-11-demo
dpkg -L openjdk-11-demo
```

日本語入力の設定
```
sudo apt install fcitx-mozc dbus-x11 x11-xserver-utils
sudo bash -c "dbus-uuidgen > /var/lib/dbus/machine-id"
sudo apt-get install fonts-takao

cd $HOME
cat << EOS >> .profile
export GTK_IM_MODULE=fcitx
export QT_IM_MODULE=fcitx
export XMODIFIERS="@im=fcitx"
export DefaultIMModule=fcitx
xset -r 49
EOS

fcitx-autostart
```

firefoxのインストール
```
sudo apt-get install firefox
sudo apt-get install firefox-locale-ja
sudo apt install fonts-ipafont
```

カーネルモジュールのコンパイル環境を構築(その1)
```
sudo bash -c "cat <<EOL>>/etc/apt/sources.list
deb-src http://archive.ubuntu.com/ubuntu bionic main
deb-src http://archive.ubuntu.com/ubuntu bionic-updates main
EOL
"
sudo apt-get update
sudo apt source linux
sudo apt-get install libelf-dev
sudo apt-get install bison
sudo apt-get install flex

sudo bash -c "cat /proc/config.gz  | gzip -dc > /usr/src/linux-4.15.0/.config"

cd /usr/src/linux-4.15.0
sudo make prepare
sudo make scripts
```

カーネルモジュールのコンパイル環境を構築(その2)
```
sudo apt-get install libelf-dev
sudo apt-get install bison
sudo apt-get install flex

cd /usr/src
sudo git clone https://github.com/microsoft/WSL2-Linux-Kernel.git
sudo bash -c "cat /proc/config.gz  | gzip -dc > /usr/src/4.19.104-microsoft-standard/.config"

cd /usr/src/4.19.104-microsoft-standard
sudo make prepare
sudo make scripts
```
操作ログを記録する
```
exec script -f /mnt/c/Users/hideki/Documents/logs/$(date +%Y-%m-%d-%H%M%S).log
```


chromeのインストール
```
sudo sh -c 'echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
curl -x http://192.168.33.4:8080 -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key adv --keyserver keyserver.ubuntu.com --keyserver-options http-proxy=http://192.168.33.4:8080 --recv-keys 78BD65473CB3BD13
sudo apt-get update
sudo apt-get install google-chrome-stable
google-chrome --proxy-server=http://192.168.33.4:8080
```

microsoft edgeをインストール
```
curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
sudo install -o root -g root -m 644 microsoft.gpg /etc/apt/trusted.gpg.d/
sudo apt update; sudo apt install microsoft-edge-dev
which microsoft-edge
microsoft-edge
```

wsl2の設定ファイル(個人用)
cat  /mnt/c/Users/${USER}/.wslconfig
```
[wsl2]
memory=2GB
localhostForwarding=True
```

ユーザーの追加
```
useradd -m -s $SHELL hideki
passwd hideki
usermod -G sudo hideki
echo hideki ALL=NOPASSWD: ALL > /etc/sudoers.d/hideki
chmod 440 /etc/sudoers.d/hideki
```

パスワードなしでsudoを実行する
```
cat <<EOL>askpass
#!/bin/bash
echo "password"
EOL
chmod 700 askpass
export SUDO_ASKPASS=$HOME/askpass
sudo -A -s
```
curlで自分のグローバルIPを調べる
```
curl httpbin.org/ip
curl inet-ip.info
```
ipアドレスと経路とresolvの設定
```
sudo ip a a 10.220.1.121/24 broadcast 10.220.1.255 dev ens18
sudo ip route add default via 10.220.1.1
sudo systemd-resolve --interface ens18 --set-dns 10.220.250.21
systemd-resolve --status
sudo hostnamectl set-hostname test-vm.mukogawa.org
```
ipアドレスの削除
```
sudo ip a del 10.220.1.121/24 dev ens18
```
時刻を設定する
```
sudo ntpdate -v ntp.nict.jp
```
時刻を設定する(ntpdが動作している場合)
```
sudo ntpdate -v -u ntp.nict.jp
```

dockerのインストール
```
sudo apt update
sudo apt install ca-certificates curl gnupg lsb-release
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt update
sudo apt install docker-ce docker-ce-cli containerd.io
sudo service docker start
sudo docker run hello-world
sudo usermod -aG docker $USER
```

共有ページメモリのサイズの合計を求める
```
top -b -n 1|sed 1,7d|awk '{t = t + $7}END{print t}'
```

ファイルシステムの1ブロックのサイズを求める
```
sudo /sbin/tune2fs -l /dev/sdc |grep 'Block size'
```

python2で作成したスクリプトをpython3に変換させる
```
pip install 2to3
2to3  vm_init.py > vm_init.py.patch
patch -p0 vm_init.py < vm_init.py.patch
```

パッケージファイルの中身をみる
```
dpkg-deb -I session-manager-plugin.deb
dpkg-deb -c session-manager-plugin.deb
```

あるファイルがどのパッケージに属しているか調査する
```
 dpkg -S /usr/share/lintian/overrides/sessionmanagerplugin
```

パッケージの全ファイル名を表示する
```
dpkg -L session-manager-plugin
```

パスワードの有効期限を調べる
```
chage -l hideki
```

パッケージのソースを取得する
```
apt source linux-libc-dev
```

再起動した日時を確認する
```
last reboot --time-format=full
  (or)
who -b
```

ダブルクオーティションで括られたcsvファイルを処理するgawkのコマンド例
```
cat test.csv | awk 'BEGIN{FPAT="([^,]*)|(\"[^\"]+\")"; OFS="\t"}($14 ~ /新.*旧/){print $4, $14}'
```

csvファイルをセパレートするコマンド例
```
cat test.csv|while read LINE; do IFS=, item=($LINE); echo ${item[1]}; done
cat test.csv |while read LINE; do IFS=, item=($LINE); i=$((i += ${item[1]})); echo $i;done
```

ext4(ファイルシステム)の情報を表示する
```
sudo tune2fs -l /dev/sdc
```

tcpdumpの確認(https)
```
sudo tcpdump -s 1500 -X -i eth0 tcp port 443 and src 192.168.1.12
```

tcpdumpの確認(ssh)
```
sudo tcpdump -s 1500 -X -i eth0 tcp port 22 and src 192.168.1.12
```

自動更新と自動削除のパッケージを調べる
```
sudo unattended-upgrade -v --dry-run
```

自動更新のスケジュールを調べる
```
cat /lib/systemd/system/apt-daily-upgrade.timer
```

ネットワークの統計情報を調べる
```
ss -s
ip -s link
```

sedで条件に一致した行を出力する
```
sed -n '/\xc2\xa0/p' test.txt
```

ホワイトスペースの繰り返しを半角スペース1文字に置換する例
```
dig @8.8.8.8 yahoo.co.jp a +nottlid|grep '^[\_A-Za-z]'|sed -z 's/[\t ]\+/ /g'
```

Disk I/O統計情報を取得する
```
iostat -d -x sdc 2
```
Disk情報を取得する
```
smartctl -a /dev/sdc
```

キャッシュをクリアする
```
echo 1 > /proc/sys/vm/drop_caches
```

スーパーブロックの内容を表示する
```
sudo dumpe2fs -h /dev/sdc
```

iノード情報を表示する
```
stat /etc/passwd
stat -c %F,%u,%g,%A,%x,%y,%z,%h,%B,%d,%i /etc/passwd
```

プロセスのwchan情報を表示する
```
ps -eo lstart,pid,args,wchan
```

ファイルに対して削除できない属性を設定する
```
chattr +i README.md
```

ファイルに対して削除できない属性を解除する
```
chattr -i README.md
```
