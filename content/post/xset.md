---
title: "xsetでdpmsをoff"
date: 2018-10-12T15:50:21+09:00
archives: "2018"
tags: [x11]
author: hidekuno
---
これで
```
xset -dpms
xset s off
```