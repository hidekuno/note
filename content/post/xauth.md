---
title: "Xauth"
date: 2020-03-16T08:16:25+09:00
archives: "2020"
tags: [x11]
author: hidekuno
---

Xアプリケーションを実行すると、下記現象が発生する場合がある。(原因は調べてるところ)
```
Invalid MIT-MAGIC-COOKIE-1 keyError: Can't open display: 172.16.43.12:0
```

その対処(暫定措置)を記す。
```
xauth list $DISPLAY
xauth remove $DISPLAY
```
