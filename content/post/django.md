---
title: "Djangoの使い方"
date: 2021-05-04T14:59:48+09:00
archives: "2021"
tags: [python]
author: hidekuno
---

djangoのインストール(ubuntu)
```
sudo apt install python3-pip
echo 'export PATH="$HOME/.local/bin:$PATH"' >> ~/.bashrc
source ~/.bashrc
pip3 install Django
pip3 install django-bootstrap4
```

プロジェクトの作成例
```
django-admin startproject ${PRJ}
```

migrateのやり方
```
python3 manage.py makemigrations
python3 manage.py migrate
```

管理者の作成
```
python3 manage.py createsuperuser
```

アプリの作成
```
python3 manage.py startapp ${APP}
```

migrateのやり方(アプリ単位)
```
python3 manage.py makemigrations ${APP}
python3 manage.py migrate ${APP}
```

modelの操作
```
python3 manage.py shell
```
