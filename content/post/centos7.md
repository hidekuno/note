---
title: "CentOS7のなんとか"
date: 2018-10-16T11:47:55+09:00
archives: "2018"
tags: [os]
author: hidekuno
---

TCPの状態
```
ss -ant
```
UDPの状態
```
ss -anu
```
TCPとUDPの状態
```
ss -anut
```
routingの表示
```
ip r
```
ARPテーブルの表示
```
ip n
```
全サービス状態を調べる
```
systemctl list-unit-files
```

ホスト名を調べる
```
hostnamectl status
```

ホスト名を変更する
```
hostnamectl set-hostname ホスト名
```

インターフィエスをUP
```
nmcli c up eno0
```
インターフィエスをDown
```
nmcli c down eno0
```
