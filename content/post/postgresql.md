---
title: "Postgresqlのあれこれ"
date: 2018-10-12T16:14:40+09:00
archives: "2018"
tags: [db]
author: John SMITH
---

全テープルの件数を取得する
```
analyze;
select relname,n_live_tup from pg_stat_user_tables order by n_live_tup;
```

ジェネレータ関数を使う
```
SELECT GENERATE_SERIES(1, 10) as no;
SELECT GENERATE_SERIES(1, 10, 2) as no;
SELECT '2019-01-01'::DATE + GENERATE_SERIES(0, 9) as mday;
```
CSV出力(テーブル編)
```
\copy jvn_cwe_work to '/tmp/jvn_cwe_work.csv' with csv DELIMITER ',' HEADER;
```
CSV出力(クエリ編)
```
psql -U hideki hideki_db -c "select identifier, title from  jvn_vulnerability where cweid is null;"  -A -F,
```
テキストファイルからテンポラリテーブルを作成する
```
CREATE TEMPORARY TABLE WORK_CHANGE (
  src_v_ip VARCHAR(15) NOT NULL,
  execution_reservation_date timestamp,
  status smallint default 10
);
\copy WORK_CHANGE FROM 'memo.txt';
```
シーケンス番号の初期化
```
select setval ('account_services_id_seq',1,false);
```
SQLの実行計画を調べる
```
EXPLAIN ANALYZE
select
 identifier, title, link, modified_date, issued_date, max(fs_manage) as fs_manage
 from
 (
   select   a.identifier, title, link, modified_date, issued_date,
          (case when modified_date < ticket_modified_date then 4
                when fs_manage='not_cover_item'           then 0
                when fs_manage='cover_item'               then 1
                when fs_manage='undefine' and edit=0      then 2
                else                                           3
           end) as fs_manage
   from     jvn_vulnerability a, jvn_vulnerability_detail b, jvn_product c
   where    a.identifier = b.identifier
   and      b.cpe = c.cpe) as mail_query
group by identifier, title, link, modified_date, issued_date order by modified_date desc limit 10;
```

ユーザーを作成
```
create role hideki with SUPERUSER LOGIN PASSWORD 'xxxxx';
```

データベースを作成
```
create database hideki_db WITH TEMPLATE = template0 OWNER = hideki encoding 'utf8' lc_collate 'ja_JP.utf8' lc_ctype 'ja_JP.utf8';
```

データベース名の変更
```
alter database kunohi_db rename to kunohi_db_old ;
alter database kunohi_db_new rename to kunohi_db ;
alter database kunohi_db rename to hideki_db ;
```

データベースの所有者の変更
```
ALTER DATABASE hideki_db OWNER TO hideki;
```

テーブルの所有者の変更
```
alter table jvn_account OWNER TO hideki;
```

タイムゾーンの確認
```
show timezone;
```

タイムゾーンの確認(一覧)
```
SELECT * FROM pg_timezone_names; 
```

タイムゾーンの変更
```
ALTER DATABASE kunohi_db SET timezone TO 'Asia/Tokyo';
```

設定内容を反映する
```
SELECT pg_reload_conf();
```

クライアント単位(接続単位)で設定する場合
```
SET timezone TO 'Asia/Tokyo';
```

ロケールの確認
```
SELECT name, setting, context FROM pg_settings WHERE name LIKE 'lc%';
```

psqlで時間を計測する
```
\timing
```

psqlでエラーをハンドルする
```
echo '/hoge;' | psql -v ON_ERROR_STOP=ON -U jvn jvn_db
```

DB全体のバックアップを行う(圧縮)
```
pg_dump -v -U jvn jvn_db | gzip -c > /tmp/jvn_dump.sql.gz
```

DB全体のバックアップを行う(テーブル単位)
```
pg_dump -U jvn jvn_db -t jvn_product > jvn_product.sql
```

最後のトランザクションIDを取得する
```
select txid_current();
```

psqlでシステム変数の取得
```
show shared_buffers;
show max_connections;
show work_mem;
show wal_buffers;
show effective_cache_size;
```
psqlでシステム変数の取得(全て)
```
show all;
```

システム変数の変更(例)
```
set standard_conforming_strings=off;
```

テーブルスペースの作成・確認
```
CREATE TABLESPACE dbspace LOCATION '/var/lib/pgsql/data';
select oid, * FROM pg_tablespace;
select pg_tablespace_location(16390);
```

トランザクションレベルを調べる
```
SHOW TRANSACTION ISOLATION LEVEL;
```

REPEATABLE READでトランザクションを開始する
```
BEGIN TRANSACTION ISOLATION LEVEL REPEATABLE READ;
```

SERIALIZABLEでトランザクションを開始する
```
BEGIN TRANSACTION ISOLATION LEVEL SERIALIZABLE;
```

接続確認(9.3以上)
```
pg_isready -h 127.0.0.1 -p 5432
```

サーバのステータスを確認
```
pg_ctl status -D /data/databases/postgresql-9.6/data
```

like検索のためのインデックス作成方法(例 char型)
```
CREATE INDEX jvn_ipaddr_idx_1 ON jvn_ipaddr (addr bpchar_pattern_ops);
```

CPU負荷をかけるテスト
```
stress --cpu 10 --timeout 10m
```

I/O帯域を絞るテスト
```
# ls -l  /dev/vd*
brw-rw---- 1 root disk 252,  0 Oct  1 10:40 /dev/vda
brw-rw---- 1 root disk 252,  1 Oct  1 10:40 /dev/vda1
brw-rw---- 1 root disk 252, 16 Oct  1 10:40 /dev/vdb
brw-rw---- 1 root disk 252, 32 Oct  1 10:40 /dev/vdc
# cat /cgroup/blkio/blkio.throttle.write_iops_device 
252:32    1000
# echo "252:32 1024" > /cgroup/blkio/blkio.throttle.read_bps_device
# echo "252:32 1024" > /cgroup/blkio/blkio.throttle.write_bps_device
#
# iostat -dtx 1
```

I/O帯域を絞るテスト(元に戻す)
```
echo "252:32 0" > /cgroup/blkio/blkio.throttle.read_bps_device
echo "252:32 0" > /cgroup/blkio/blkio.throttle.write_bps_device 
```

IOPS(1s当りの回数)を絞るテスト
```
echo "252:32 1" > /cgroup/blkio/blkio.throttle.write_iops_device
echo "252:32 1" > /cgroup/blkio/blkio.throttle.read_iops_device
```

IOPS(1s当りの回数)を元に戻す
```
echo "252:32 0" >  /cgroup/blkio/blkio.throttle.write_iops_device
echo "252:32 0" >  /cgroup/blkio/blkio.throttle.read_iops_device
```
### 公式dockerイメージを日本語化する
#### Dockerfileの中身
```
FROM postgres:9.2
RUN localedef -i ja_JP -c -f UTF-8 -A /usr/share/locale/locale.alias ja_JP.UTF-8
ADD jvn_init.sql /docker-entrypoint-initdb.d/
ENV LANG ja_JP.utf8
```
#### 日本語化したイメージを作成する
```
docker build -t postgres:jvn .
```

#### psqlで日本語入力するには
```
psql -n -U jvn jvn_db
```
