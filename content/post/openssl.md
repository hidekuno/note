---
title: "Openssl"
date: 2019-10-11T14:00:53+09:00
archives: "2019"
tags: [openssl]
author: John SMITH
---
暗号アルゴリズムの一覧
```
openssl ciphers 'ALL'
openssl list -cipher-algorithms
```
暗号アルゴリズムの一覧(公開鍵)
```
openssl list -public-key-algorithms
```
設定内容の確認
```
openssl ciphers -v 'HIGH:!aNULL'
```

```
./ssl.h:#define SSL_DEFAULT_CIPHER_LIST "AES:ALL:!aNULL:!eNULL:+RC4:@STRENGTH" /* low priority for RC4 */
```
centosでのテスト(例)
```
openssl s_server -debug -msg -cipher 'AES256-SHA:AES128-SHA' -accept 9999 -no_ssl2 -no_ssl3 -www -cert /etc/pki/tls/certs/server.crt  -key /etc/pki/tls/private/server.key
```
```
openssl s_client -connect localhost:9999 -cipher RC4-MD5
```

プロキシーサーバ環境での使用例
```
openssl s_client -proxy 192.168.33.4:8080  -servername apps.powerapps.com  -connect apps.powerapps.com:443 -tls1_2
```

サーバー証明書を取得する
```
openssl s_client -connect hoge.ne.jp:443 -showcerts </dev/null | openssl x509 -text -noout
```

データの暗号化・復号化(パスワード入力)
```
openssl enc -e -aes-256-cbc -salt -k $PASSWORD -in data.tar.gz  -out data.dat
openssl enc -d -aes-256-cbc -salt -k $PASSWORD -in data.dat     -out data1.tar.gz
```

パスワードファイルを公開鍵方式で暗号化・復号化
```
openssl genrsa 4096 > private-key.pem
openssl rsa -in private-key.pem -pubout -out public-key.pem
openssl rsautl -encrypt -pubin -inkey public-key.pem -in passwd.txt -out passwd.encrypted
openssl rsautl -decrypt -inkey private-key.pem -in passwd.encrypted
```

データの暗号化・復号化(パスワードファイル)

```
openssl enc -e -aes-256-cbc -pbkdf2 -in fj.dat -out fj.encrypted -pass file:passwd.txt
openssl enc -d -aes-256-cbc -pbkdf2 -in fj.encrypted -pass file:passwd.txt
```

ファイルの電子署名と検証
```
openssl dgst -sha256 -sign private-key.pem fj.dat > sign.sig
openssl dgst -sha256 -verify public-key.pem -signature sign.sig fj.dat

```

プロトコルの安全強度を調べる
```
nmap --script ssl-enum-ciphers -p 443 www.yahoo.co.jp
```

パスワードをsha512で暗号化する
```
openssl passwd -6 -salt=salt 'passwordnanika'
```

AWSのキーペアのフィンガープリントを表示する
```
openssl pkcs8 -in private-key.pem -inform PEM -outform DER -topk8 -nocrypt | openssl sha1 -c
```

pem形式の秘密鍵の内容を確認する
```
openssl rsa -in private-key.pem -text -noout
```

証明書のフィンガープリントを確認する
```
openssl x509 -sha1 -fingerprint -in hogehoge.cer -noout
```

DKIMで使用する公開鍵のビット長を調べる
```
dig dmkhcugko3o5nkjv67jqit576afxvzy4._domainkey.amazon.co.jp txt +short|grep '^"p='|sed -e 's/.p=//' -e 's/.$//'| awk '{print "-----BEGIN PUBLIC KEY-----"; print $0; print "-----END PUBLIC KEY-----"}'| openssl rsa -text -pubin
```

サイトに設置している証明書の期限を調べる(例:www.yahoo.co.jp)
```
openssl s_client -connect www.yahoo.co.jp:443 2> /dev/null </dev/null|awk '(/BEGIN CERTIFICATE/){flg = 1;}(flg == 1){print}(/END CERTIFICATE/){flg=0}' | openssl x509 -startdate -enddate -noout
```
