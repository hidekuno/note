---
title: "認証局(CA)構築"
date: 2018-10-12T15:19:35+09:00
archives: "2018"
tags: [ssl]
author: hidekuno
---

## 概要手順
1. 新しく認証局を作成する (発行者 目線)
    1. 事前設定
        - openssl.conf -> CA用に編集する
        - index.txt    -> touchコマンドで初期化する
        - serial       -> シリアルを初期化します。 
    1. CA証明書/秘密鍵作成 (自己署名済みの証明書と秘密鍵を作成する)  

1. 鍵ペアと証明書要求(CSR)ファイルを作成する (サーバ運営者 目線)  
```
   openssl req -new -keyout key.pem (秘密鍵ファイル) -out csr.pem(証明書要求(CSR)ファイル)
```

1. 自己認証CA局で署名(発行者 目線)  
```
openssl ca -out /etc/ssl/www.hoge.com/hoge_cert.pem -infiles /etc/ssl/www.hoge.com/hoge_csr.pem
```

#### Symantecの認証局を作る
```
CN=Symantec Class 3 Secure Server CA - G4
O=Symantec Corporation
OU=Symantec Trust Network
```

## 詳細手順
```
cd /home/kunohi
mkdir -p cert/symantec
OPENSSLDIR=`/usr/bin/openssl version -a|grep OPENSSLDIR | sed -e 's/"//g' -e 's/^.* //'`
cd $HOME/cert/symantec
mkdir -p CA
mkdir -p CA/misc

cp -p ${OPENSSLDIR}/openssl.cnf CA/.
cp -p ${OPENSSLDIR}/openssl.cnf CA/openssl-server.cnf
cp -p ${OPENSSLDIR}/misc/CA CA/misc/CA.sh

export CATOP=${HOME}/cert/symantec/CA
export SSLEAY_CONFIG="-config $CATOP/openssl.cnf"

vi ${CATOP}/openssl.cnf
+----------------------------------------------------+
dir = /home/kunohi/cert/symantec/CA
default_md = sha256
default_bits = 4096
countryName_default           = JP
stateOrProvinceName_default   = Osaka
localityName_default  = Osaka-shi
0.organizationName_default    = Symantec Corporation
organizationalUnitName_default        = Symantec Trust Network
commonName_default  = Symantec Class 3 Secure Server CA - G4
basicConstraints=CA:TRUE
nsCertType                    = sslCA, emailCA
keyUsage = cRLSign,keyCertSign
+----------------------------------------------------+

vi ${CATOP}/openssl-server.cnf
+----------------------------------------------------+
dir = /home/kunohi/cert/symantec/CA
default_md = sha256
default_bits = 4096
countryName_default           = JP
stateOrProvinceName_default   = Osaka
nsCertType                    = server
basicConstraints = CA:FALSE                                   ←確認
keyUsage = nonRepudiation, digitalSignature, keyEncipherment  ←確認
+----------------------------------------------------+

cd ${CATOP}
echo "01" > serial
```

#### CA証明書/秘密鍵作成 (自己署名済みの証明書と秘密鍵を作成する)
```
./misc/CA.sh -newca
cacert.pem           ルート証明書
private/cakey.pem    秘密鍵
```

#### CASymantecの認証CA局で署名
```
export CATOP=${HOME}/cert/symantec/CA
cd ${CATOP}

(A)の証明書有効期間
CSR=$HOME/cert/csr/opepotest.jp.csr
CERT=$HOME/cert/csr/opepotest.jp.cert
/usr/bin/openssl ca -policy policy_anything -config openssl-server.cnf -in $CSR -out $CERT -startdate 180420000000Z -enddate 190420000000Z
```

### 自己認証局(CA)の証明書更新手順

(参考) http://server-setting.info/centos/private-ca-cert.html#openssl_8

- 認証局の有効期限を更新する。
- サーバー、クライアントの証明書に関しては、有効期限が切れたら、失効、再発行の手順で行います。
- 認証局の場合に限っては、更新作業を行います。
- もちろん、更新したからといっても新しい認証局の証明書を再配布する必要はあります。
- 以下に、現在の証局の有効期限を確認し、10年まで期限を延ばしてみます。

確認
```
/usr/local/ssl/bin/openssl x509 -inform PEM -in cacert.pem -noout -text
```

更新
```
/usr/local/ssl/bin/openssl req -new -x509 -days 3650 -key private/cakey.pem -sha256 -out cacert.pem -config ./openssl.cnf
```

確認
```
/usr/local/ssl/bin/openssl x509 -in cacert.pem -outform DER -out cacert.der
```

