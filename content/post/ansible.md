---
title: "Ansible"
date: 2021-08-19T13:28:16+09:00
archives: "2021"
tags: [os]
author: hidekuno
---

sudo vi /etc/ansible/hosts
```
[dev]
localhost
```

sudo vi /etc/ansible/ansible.cfg
```
[defaults]
interpreter_python = /usr/bin/python
pipelining = True

[ssh_connection]
ssh_args = -o ControlMaster=auto -o ControlPersist=60s -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null
```

テスト用コマンド
```
ansible dev -m ping -o -u hidekuno
ansible dev -m setup -o -u hidekuno
```
```
ansible -i inventories/dev ohap -m ping -o -u hideki
ansible -i inventories/dev ohap -m setup -o -u hideki
```

Playbookの実行
```
ansible-playbook -i inventories -u hideki playbook.yml
```

モジュールの一覧を取得
```
ansible-doc -l
```
