---
title: "Freebsd"
date: 2024-08-16T17:26:25+09:00
tags: [os]
draft: true
---

拡張ファイル属性を表示する
```
ls -ol
```

削除不可に設定する
```
chflags nouunlnk testfile
```

カーネルパラメータを設定する
```
sudo sysctl vfs.bdwriteskip=1
```

カーネルトレースを実行する
```
ktrace -f result.out mv test.dat data.dat
kdump -f result.out
```

システムコールの実行回数を表示する
```
truss -c ./a.out
truss -c php test.php
```

プログラムのトレースを実行する
```
sudo dtrace -F -s test.d -c "php test.php"
sudo dtrace -F -s sys.d -p 781
```

Dtraceでprobeできるプロバイダを表示する
```
dwatch -l
```

スーパーブロックの内容を表示する
```
sudo dumpfs -s /dev/da0p2
```

UFSの状態を表示する
```
sudo tunefs -p /dev/da0p2
```

マウントの内容を表示する
```
mount | grep /dev/da0p2
```

カーネルモジュールを登録する
```
sudo kldload ./counter.ko
```

動作しているカーネルモジュールを表示する
```
kldstat
```

カーネルモジュールを削除する
```
sudo kldunload counter.ko
```

Disk I/Oの稼働状況を表示する
```
iostat -d -x da0 2
```

ハードウェア構成情報を調べる
```
less /var/run/dmesg.bootq
```

Disk情報を調べる
```
dmesg | grep da0
```
```
sudo camcontrol inquiry da0
```
```
sudo smartctl -a /dev/da0
```
パーティション情報を調べる
```
gpart show da0
```

同期出力でファイルを作成する
```
dd oflag=sync if=/dev/urandom of=test.dat bs=1M count=100
```

TCPコネクションを切断する
```
tcpdrop 192.168.32.154 22 192.168.32.1 53124
```

コネクション情報（TCP UDP UnixDomain）より、どのプロセスがポート番号を使用しているか調べる
```
sockstat
```

NFSとしてマウントする
```
sudo mount -t nfs -o nfsv4 192.168.32.152:/home/nfsshare /mnt
```

プロセスの開始時刻, wchanを表示する
```
ps -auxweo lstart,wchan
```
