---
title: "Emacs"
date: 2021-01-13T17:01:18+09:00
tags: [develop]
author: hidekuno
---

### よく忘れる操作
- カーソル移動

|操作|キーバインド|コマンド|
|---|---|---|
|指定行へ移動|M-g M-g, M-g g|goto-line|
|指定列へ移動|M-g TAB|move-to-column|
|ウィンドウの最上部に|M-r M-r|move-to-window-line|
|ウィンドウの中央に|M-r|move-to-window-line|
|ウィンドウの最下部に|M-r M-r M-r|move-to-window-line|

- 入力・編集

|操作|キーバインド|コマンド|
|---|---|---|
|直前のコマンドを繰り返す|C-x z|repeat|
|すべてを選択|C-x h||
|直前のコマンドを繰り返す|C-x z|repeat|
|特殊文字の入力|C-q {char}|quoted-insert|
|行末の空白を削除する||delete-trailing-whitespace|

- マクロ

|操作|キーバインド|コマンド|
|---|---|---|
|記録開始|C-x (|start-kbd-macro|
|記録終了|C-x )|end-kbd-macro|
|実行|C-x e|call-last-kbd-macro|
|編集|C-x C-k|edit-kbd-macro|

- キーバインドの一覧を表示
```
describe-bindings
```

- 指定したディレクトリ以下のファイルに対して、一括置換を行う
```
M-x find-name-dired
Find-name (directory): ~/path/
Find-name (filename wildcard): *

t
Q
Query replace regexp in marked files: foo
Query replace regexp in marked files foo with: bar
C-x s
!
```

- diredでリモートのディレクトリをオープンする
```
C-x C-f
Find file: /ssh:${host}:${dir} 
```

- 「Failed to verify signature archive-contents.sig:」の対処方法
```
gpg --keyserver hkp://keyserver.ubuntu.com:80 --homedir ~/.emacs.d/elpa/gnupg/ --receive-keys XXXXXXXXXXXXXXXX
```

- etagsを作成する
```
cd freebsd-src
find . -name "*.[ch]"|etags -
```
|操作|キーバインド|コマンド|
|---|---|---|
|TAGSファイル読み込み||visit-tags-table|
|指定の関数へJump|M-.|xref-find-definitions|
|元に戻る|M-,|xref-pop-marker-stack|
|関数を検索する||tags-search|
|関数を続けて検索する||tags-loop-continue|

