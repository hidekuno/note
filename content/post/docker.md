---
title: "Docker"
date: 2019-10-04T13:42:34+09:00
archives: "2019"
tags: [develop]
author: hidekuno
---

ネットワークの一覧表示
```
docker network ls
```

ネットワークの確認
```
docker network inspect bridge 
```

未使用のvolumeを確認する
```
docker volume ls -f "dangling=true"
```

未使用のvolumeを削除する
```
docker volume ls -f "dangling=true" |sed 1d |awk '{print $2}'|xargs docker volume rm
```

Docker Imageの履歴を確認する
```
docker history hidekuno/rust-elisp:latest
```

Docker Imageの変更を確認する
```
docker diff jvn_plt
```

buildキャッシュをクリアする
```
docker builder prune
```

使用容量を確認する
```
docker system df
```

wslのイメージ・コンテナファイルの場所を移動する
```
wsl --export docker-desktop-data docker-desktop-data.tar
wsl --unregister docker-desktop-data
wsl --import docker-desktop-data D:\docker docker-desktop-data.tar
```

githubでforkしたリポジトリにオリジナルリポジトリの最新内容を反映させる
```
git clone https://github.com/hidekuno/go-gtk
cd go-gtk/
git remote add upstream git://github.com/mattn/go-gtk.git
git remote -v
git fetch upstream
git branch -a
git checkout master
git merge upstream/master
git push origin master
```
docker コンテナを保存する
```
docker export jvn_web| gzip -c > /tmp/jvn_web.tar.gz
```

docker コンテナをimport
```
gzip -dc /tmp/jvn_web.tar.gz | docker import - jvn_web:latest
```

dockerコンテナを実行
```
docker run --name zabbix-appliance -t -p 10051:10051 -p 80:80 -p 443:443 -d zabbix/zabbix-appliance:latest
```
任意のネットワークにdockerコンテナを接続させる
```
docker network connect  --ip 192.168.2.20 service-manage-infra zabbix-appliance
```
任意のネットワークからdockerコンテナを切断させる
```
docker network disconnect bridge zabbix-appliance
```

ubuntuにdockerをインストールする手順
```
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin
sudo apt-get install docker-compose
sudo service docker start
docker run hello-world
```
