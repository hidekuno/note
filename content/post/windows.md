---
title: "Windows作業記録"
date: 2024-04-07T15:20:49+09:00
archives: "2020"
tags: [os]
author: "hidekuno"
---

wsl2で起動したsshサーバを外部から接続可能にするスクリプト(windows側で設定する)
```
IP=`ip -4 address show  eth0 |grep inet|sed  's/^ *inet //' | cut -d/ -f1`

netsh.exe interface portproxy delete v4tov4 listenport=22
netsh.exe interface portproxy add    v4tov4 listenport=22 connectaddress=$IP
```

固定IPで起動したsshサーバを外部から接続可能にするスクリプト(windows側で設定する)
```
wsl -d Ubuntu-20.04 -u root ip addr add 192.168.127.16/24 broadcast 192.168.127.255 dev eth0 label eth0:1
netsh.exe interface ip add address "vEthernet (WSL)" 192.168.127.1 255.255.255.0
netsh.exe interface portproxy delete v4tov4 listenport=22
netsh.exe interface portproxy add v4tov4 listenport=22 listenaddress=0.0.0.0 connectport=22 connectaddress=192.168.127.16
sc.exe config  iphlpsvc start=auto
sc.exe start iphlpsvc
```

シリアル番号を表示する
```
wmic.exe csproduct get identifyingnumber| tr -d '\015' | sed -e '1d' -e '/^[ ]*$/d' -e 's/ *$//'
```

openssh-serverをインストールする
```
Get-WindowsCapability -Online | Where-Object Name -like 'OpenSSH*'
Add-WindowsCapability -Online -Name OpenSSH.Server~~~~0.0.1.0
Get-Service -Name sshd
Set-Service sshd  -StartupType Manual
Start-Service sshd
```

CORSを許可する状態でchromeを起動する
```
"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe" --disable-web-security --user-data-dir=D:\tmp
```

DNSキャッシュをクリアする
```
ipconfig /flushdns
```
