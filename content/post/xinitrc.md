---
title: "Xinitrc"
date: 2018-11-09T16:25:08+09:00
archives: "2018"
tags: [x11]
author: hidekuno
---

```
[kunohi@centos7-dev-docker ~]$ cat ~/.xinitrc
export LANG="ja_JP.UTF-8"
export XMODIFIERS="@im=ibus"
export XMODIFIER="@im=ibus"
export GTK_IM_MODULE=ibus
export QT_IM_MODULE=ibus
export DefaultIMModule=ibus
ibus-daemon -drx

xclock -geometry -0+0 &
xeyes  -geometry +0+0 &
xterm  -geometry 80x64-0+168 -e top &
xset -dpms
xset s off
exec mwm
[kunohi@centos7-dev-docker ~]$ 
```
