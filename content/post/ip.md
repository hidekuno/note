---
title: "IP"
date: 2022-02-02T13:28:16+09:00
archives: "2021"
tags: [net]
author: hidekuno
---

ネットワークデバイスの確認
```
sudo lshw -short -class network
```

ネットワークインターフェイスを追加
```
sudo ip addr add 192.168.127.16/24 broadcast 192.168.127.255 dev eth0 label eth0:1
```
ネットワークインターフェイスを追加
sudo vi /etc/network/interfaces
```
auto eth0:1
iface eth0:1 inet static
address 192.168.127.16
network 192.168.127.0
netmask 255.255.255.0
broadcast 192.168.127.255
```

vxlanで疎通の確認を行う(host A)
```
ip link add vxlan0 type vxlan id 10 dstport 4789 dev enX0
ip -d link show vxlan0
ip link set up vxlan0
ip addr add 172.24.77.191/24 dev vxlan0
ip addr show vxlan0
bridge fdb append 00:00:00:00:00:00 dev vxlan0 dst 10.220.1.78
ping 172.24.77.78
ip link delete vxlan0
```

vxlanで疎通の確認を行う(host B)
```
ip link add vxlan0 type vxlan id 10 dstport 4789 dev enX0
ip -d link show vxlan0
ip link set up vxlan0
ip addr add 172.24.77.78/24 dev vxlan0
ip addr show vxlan0
bridge fdb append 00:00:00:00:00:00 dev vxlan0 dst 10.220.1.191
tcpdump -i enX0 udp port 4789 -n -v
ip link delete vxlan0
```

ipv6のアドレスを追加
```
sudo ip -6 addr add fda6:eacc:b448:1::2/64 dev eth0
```

ipv6のアドレスを追加(MacOS)
```
sudo ifconfig en0 inet6 add fda6:eacc:b448:1::3/64 prefixlen 64
```

