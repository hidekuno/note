---
title: "Hugo"
date: 2018-11-09T16:29:37+09:00
archives: "2018"
tags: [hugo]
author: hideki kuno
---

初期化
```
mkdir $HOME/hugo
cd  $HOME/hugo
hugo new site hidekuno
```

記事の投稿
```
cd $HOME/hugo/hidekuno
hugo new post/hugo.md
```

ローカルサーバを立ち上げ、確認する
```
cd $HOME/hugo/hidekuno
hugo server
```

サイトの構築
```
cd $HOME/hugo/hidekuno
hugo
```

差分を確認
```
cd $HOME/hugo/hidekuno
rsync --dry-run -acv public/ ~/github.io/
```

差分コピー
```
cd $HOME/hugo/hidekuno
rsync -acv public/ ~/github.io/
```

