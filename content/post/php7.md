---
title: "PHP7のインストール"
date: 2018-10-12T15:04:25+09:00
archives: "2018"
tags: [php,rpm,nginx]
author: hidekuno
---

epel,remiのインストール
```
yum install epel-release
rpm -Uvh http://rpms.famillecollet.com/enterprise/remi-release-6.rpm
yum install --enablerepo=remi,remi-php71 php php-devel php-mbstring php-mcrypt php-gd php-xml php-intl php-pecl-xdebug php-bcmath php-mysqlnd php-zip
```

nginxでマルチドメインの設定
```
cd /etc/pki/tls/certs
make develop.crt
openssl rsa -in develop.key -out develop.key
openssl x509 -in develop.crt -noout -text
vi /etc/nginx/conf.d/default.conf 
+-----------------------------------------------------+
listen       443 ssl;
ssl_certificate      /etc/pki/tls/certs/develop.crt;
ssl_certificate_key  /etc/pki/tls/certs/develop.key;
+-----------------------------------------------------+

make sni.crt
yum -y install --enablerepo=remi php-fpm
yum  install --enablerepo=remi php71-php-fpm
vi /etc/opt/remi/php71/php-fpm.d/www.conf
/etc/init.d/php71-php-fpm start
```
cat /etc/hosts
```
[kunohi@centos7-dev-docker ~]$ cat /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
192.168.122.180 samba-ad
192.168.122.15  centos6-test.jp centos6-sni.jp
[kunohi@centos7-dev-docker ~]$ 
```

cat /etc/nginx/conf.d/ssl.conf 
```
server {
        listen 443 default;
        server_name centos6-test.jp;

        root html;
        index index.html index.htm;

        ssl on;
        ssl_certificate      /etc/pki/tls/certs/develop.crt;
        ssl_certificate_key  /etc/pki/tls/certs/develop.key;
        location / {
                default_type 'text/plain';
        }
        location ~ \.php$ {
                root /usr/share/nginx/html/;
                fastcgi_pass   127.0.0.1:9000;
                fastcgi_index  index.php;
                fastcgi_param  SCRIPT_FILENAME  /usr/share/nginx/html$fastcgi_script_name;
                include        fastcgi_params;
        }
}

server {
        listen 443;
        server_name centos6-sni.jp;

        root html;
        index index.html index.htm;

        ssl on;
        ssl_certificate      /etc/pki/tls/certs/sni.crt;
        ssl_certificate_key  /etc/pki/tls/certs/sni.key;
        location / {
                default_type 'text/plain';
        }
        location ~ \.php$ {
                root /usr/share/nginx/html/;
                fastcgi_pass   127.0.0.1:9000;
                fastcgi_index  index.php;
                fastcgi_param  SCRIPT_FILENAME  /usr/share/nginx/html$fastcgi_script_name;
                include        fastcgi_params;
        }
}
```

