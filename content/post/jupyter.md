---
title: "Jupyterを触ってみた"
date: 2018-11-21T14:37:12+09:00
archives: "2018"
tags: [python]
author: hidekuno
---

1. パスワードの取得
```
from notebook.auth import passwd
passwd()
```

2. 1.で取得したパスワードを下記のように設定する
```
[kunohi@centos7-dev-docker ~]$ grep -v ^#  ~/.jupyter/jupyter_notebook_config.py  | sed '/^[ ]*$/d'
c.NotebookApp.password = 'sha1:7fa38bcda00b:fe17d34fc37f34096859cb09abc8e7695c2142bd'
[kunohi@centos7-dev-docker ~]$ 
```

3. jupyterを下記ようにバックグランドで起動する(5.7より ip=*からip=0.0.0.0に変更された
```
[kunohi@centos7-dev-docker ~]$ jupyter-notebook --ip=0.0.0.0 --no-browser
```
