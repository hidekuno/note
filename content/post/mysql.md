---
title: "MySQL"
date: 2018-12-19T17:01:46+09:00
archives: "2018"
tags: [db]
author: hidekuno
---

文字コードの確認
```
show variables like 'character%';
```

テーブルのエンジンを確認
```
show table status from zl_wp_sample;
```

#### パスワードなしでログイン

/etc/my.cnfに下記内容を追記する
```
[mysqld]
skip-grant-tables
```

5.0の場合
```
mysql -h 127.0.0.1 -P 3307  -u root -p
mysql> update mysql.user set password=password('hogehoge') where user = 'root';
```

5.7の場合
```
mysql -h 127.0.0.1 -P 3307  -u root -p
mysql> update mysql.user set authentication_string=password('hogehoge') where user = 'root';
```

データベースの削除、作成、言語の設定
```
drop database wtest;
create database wtest;
alter database wtest default character set=utf8mb4;
```

mysql コマンドラインで実行
```
mysql -uroot -phogehoge -h 127.0.0.1 -e "drop database wordpress;"
mysql -uroot -phogehoge -h 127.0.0.1 -e "create database wordpress;"
mysql -uroot -phogehoge -h 127.0.0.1 -e "alter database wordpress default character set=utf8mb4;"
```

mysqldumpの実行例
```
mysqldump --single-transaction -u root -h 127.0.0.1 -p wordpress > /tmp/wordpress-5.0.1-ja.sql
mysqldump --single-transaction --no-create-info -u root -h 127.0.0.1 -p wordpress > /tmp/wordpress-5.0.1-ja.sql
mysqldump  -h 127.0.0.1  -P 33306 -u root --no-data cidr -p > schema.sql
```

query logの設定
```
SET GLOBAL slow_query_log = 'ON' ;
SET GLOBAL long_query_time = 5.0 ;
SHOW GLOBAL VARIABLES LIKE '%query%' ;
```

queryの分析
```
explain select count(*) from parent left join child on parent.ied = child.parent_id;
```

sqlモードの確認
```
SELECT @@GLOBAL.sql_mode;
SELECT @@SESSION.sql_mode;
```

sqlモードの設定
```
SET SESSION sql_mode = 'STRICT_ALL_TABLES,ONLY_FULL_GROUP_BY,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
```

テーブルの内容の確認
```
show columns from jobs;
```

権限の付与
```
GRANT all ON *.* TO root@'192.168.1.%' IDENTIFIED BY 'password';
GRANT all ON *.* TO root@'127.0.0.1' IDENTIFIED BY 'password';
FLUSH PRIVILEGES;
```

権限の付与(grantの実行権限付)
```
GRANT all ON *.* TO root@'192.168.1.%' IDENTIFIED BY 'password' WITH GRANT OPTION;
GRANT all ON *.* TO root@'127.0.0.1' IDENTIFIED BY 'password' WITH GRANT OPTION;
FLUSH PRIVILEGES;
```

#### grant実行権限の復旧
```
update mysql.user set Grant_priv='Y' where user='root';
```

sqlite3からMySQLへデータを移行する
```
sqlite3mysql -f database.cidr -h 127.0.0.1 -P 33306 -u cidr -d cidr -p
```

インデックスの一覧を取得する
```
select table_schema,table_name,index_name,column_name,seq_in_index from information_schema.statistics where table_schema = "cidr";
```
