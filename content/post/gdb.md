---
title: "gdbのメモ"
date: 2018-10-12T16:57:15+09:00
archives: "2018"
tags: [develop]
author: hidekuno
---

レジスタの確認
```
info registers
```

グローバル変数の確認
```
info variables
```

バイト単位でhex出力
```
x/16bx $esp
```

ワード単位でhex出力(i386では4バイト)
```
x/16wx $esp
```

メモリの内容を出力
```
x 0x08049268
```

文字列を表示
```
x/s 0x08049268
```

ブレークポイントの一覧
```
info breakpoints 
```

ブレークポイントの削除
```
delete [番号]
(例) delete 2
```

条件付きブレークポイントの設定
```
b 行数 if [変数:レジスタ] == [値]
(例) b 161 if $ecx == 4
```
