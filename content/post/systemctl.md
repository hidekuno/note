---
title: systemctl
date: 2023-07-12T15:50:21+09:00
archives: "2018"
tags: [os]
author: hidekuno
---

サービスを調べる(例)
```
systemctl list-units sshd*
systemctl list-units named*
```

ログを調べる(例)
```
journalctl -f -u named.service
journalctl -f -u sshd.service
```

ページャでログを調べる(例)
```
journalctl -ex -u sshd.service
```

稼働しているサービスの一覧を表示する
```
systemctl list-units
```

全てのサービスの一覧を表示する
```
systemctl list-unit-files
```

サービスを起動する(例)
```
systemctl start docker
```

サービスを停止する(例)
```
systemctl stop docker
```

サービスを自動起動できるように設定する(例)
```
systemctl enable docker
```

サービスを再起動する(例)
```
sudo service ssh restart
```

サービスの状態を調べる
```
sudo systemctl status ssh
```
