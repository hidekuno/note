---
title: "sshの設定"
date: 2018-11-12T15:43:52+09:00
archives: "2018"
tags: [ssh,net]
author: hidekuno
---

networking setup for vmware
```
sudo firewall-cmd --add-masquerade --zone=public   --permanent
sudo firewall-cmd --zone=public --add-port=443/tcp --permanent 
sudo firewall-cmd --zone=public --add-port=902/tcp --permanent 
sudo firewall-cmd --zone=public --add-port=903/tcp --permanent 

sudo firewall-cmd --add-rich-rule="rule family="ipv4" source address="172.16.43.0/24" accept"  --permanent
sudo firewall-cmd --add-rich-rule='rule family="ipv4" source address="172.16.43.0/24" forward-port port="443" protocol="tcp" to-port="1443"' --permanent
sudo firewall-cmd --add-rich-rule='rule family="ipv4" source address="172.16.43.0/24" forward-port port="902" protocol="tcp" to-port="1902"' --permanent
sudo firewall-cmd --add-rich-rule='rule family="ipv4" source address="172.16.43.0/24" forward-port port="903" protocol="tcp" to-port="1903"' --permanent
sudo firewall-cmd --reload
sudo firewall-cmd --list-all
```

cat  ~/.ssh/config
```
Host fumidai
   Hostname gateway.mukogawa.or.jp
   IdentityFile    ~/.ssh/id_dsa
   GatewayPorts    yes
   LocalForward    1443    localhost:1443
   LocalForward    1902    localhost:1902
   LocalForward    1903    localhost:1903
   User hideki
```

VMWareで利用するサービス(902,903,443)に対して、多段階にポートフォワードを設定する
```
ssh fumidai
ssh -t -L 1902:127.0.0.1:1902 -L 1903:127.0.0.1:1903 -L 1443:127.0.0.1:1443 -A 164.46.176.1 \
ssh -L 1902:192.168.66.218:902 -L 1903:192.168.66.218:903 -L 1443:192.168.66.218:443 -A kuno@164.46.15.36
```

ログイン先のpostgresqlサーバ(15432)をローカルの5432にポート転送を行う
```
ssh -L 5432:127.0.0.1:15432 -A -Y 172.16.43.12
```

sshでrsyncを利用する
```
rsync -e 'ssh -F .ssh/vm_old2.config' --dry-run -acv  192.168.1.91:chef_test/ chef_test/
```

SSHが切断される症状の対処として、Server側で/etc/ssh/sshd_configを下記のように編集する
```
ClientAliveInterval 120
ClientAliveCountMax 3
```

秘密鍵の作成例(RSA)
```
ssh-keygen -b 4096 -t rsa -N xxxxxxxx -C wsl -f ~/.ssh/id_rsa
```

秘密鍵の作成例(ed22519)
```
ssh-keygen -t ed25519 -C wsl -f id_ed22519
```

公開鍵の出力(authorized_key用)
```
ssh-keygen -y -f ~/.ssh/id_rsa
```

公開鍵のフォーマット変更(openssl用)
```
ssh-keygen -e -m pkcs8 -f id_rsa
```

フィンガープリント(指紋の出力) もしくは鍵長を調べる
```
ssh-keygen -l -E sha256 -f ~/.ssh/id_rsa
```

パスワードを変更する
```
ssh-keygen -p -f ~/.ssh/id_rsa  -P xxxxxxxx
```

sshエージェントの使用
```
# eval `ssh-agent`
#env|grep ^SSH
SSH_AUTH_SOCK=/tmp/ssh-drInKtRj79wN/agent.427
SSH_AGENT_PID=428
# ssh-add ~/.ssh/id_rsa
Enter passphrase for /home/hideki/.ssh/id_rsa:
#
```

何も実行せずに、backgroundでsocks5をオープンする
```
ssh -o 'GatewayPorts yes' -fN -D 15786 gateway-host
```

プロキシーとしてsock5を使用する
```
sudo apt install connect-proxy
```
```
Host private-gitlab
  Hostname gitlab.private.jp
  User git
  IdentityFile ~/.ssh/id_rsa
  ProxyCommand connect -S localhost:1080 %h %p
```

socks5をオープンでSMBをトンネリングする
```
 ssh -fN -D 15786 -L 1390:fileserver.ne.jp:139 gateway-host
 curl -x socks5h://0:15786 https://office.org/
```
ファイルサーバーをマウントする
```
sudo apt install cifs-utils
sudo mkdir -p /mnt/cifs
sudo chown hideki:hideki
sudo mount -t cifs -o username=xxxxxx,password=xxxxx,domain=mukogawa.or.jp,port=1390,iocharset=utf8,uid=hideki,gid=hideki,ro //localhost/data /mnt/cifs
```

SSH クライアントが強制的に非推奨の DSA アルゴリズムを受け入れるようにする
```
ssh -oHostKeyAlgorithms=+ssh-dss 192.168.1.1
```

github.comへの接続を確認
```
ssh -T git@github.com
```

リモートホストに対してsudoを実行する例
```
ssh ${RHOST} bash -c \'SUDO_ASKPASS=/home/hogehoge/askpass sudo -A usermod -p $PASS guest\'
```

パスワード認証で自動でログインする
```
sshpass -p $PASS ssh ${RUSER}@${RHOST} ps auxwwf
```

sudoでsshのエージェントフォワードを使用する
```
sudo SSH_AUTH_SOCK=$SSH_AUTH_SOCK -A ssh -A $HOST
```

socks5経由でsftpを使う
```
sftp -o ProxyCommand="/usr/bin/connect -S localhost:15786 %h %p" -i .ssh/${key} ${user}@${host}
```
