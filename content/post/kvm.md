---
title: "KVMについて記録"
date: 2018-10-12T14:39:04+09:00
archives: "2018"
tags: [os]
author: "hidekuno"
---

centos7のインストール
```
sudo virt-install \
  --name centos7 \
  --hvm \
  --virt-type kvm \
  --ram 1024 \
  --vcpus 1 \
  --arch x86_64 \
  --os-type linux \
  --os-variant rhel7 \
  --network network=default \
  --noautoconsole \
  --disk path=/var/lib/libvirt/images/centos7.img,size=10,sparse=true \
  --cdrom /var/lib/libvirt/images/CentOS-7-x86_64-Minimal-1708.iso
```

VMの開始・停止
```
sudo virsh list --all
sudo virsh start centos7
sudo virsh shutdown centos7
```

KVMの名前を変更する(centos7→centos7-samba4)
```
[kunohi@centos7-dev-docker ~]$ uuidgen
b5e61473-7b56-4473-a964-a1c4ee7edac7
[kunohi@centos7-dev-docker ~]$

virsh edit centos7
+------------------------------------------------------------------------+
|  <name>centos7-samba4</name>                                           |
|  <uuid>b5e61473-7b56-4473-a964-a1c4ee7edac7</uuid>                     |
|      <source file='/var/lib/libvirt/images/centos7-samba4.img'/>       |
+------------------------------------------------------------------------+

[kunohi@centos7-dev-docker ~]$ sudo virsh edit centos7
ドメイン centos7-samba4 XML の設定は編集されました 

[kunohi@centos7-dev-docker ~]$ sudo cp /var/lib/libvirt/images/centos7.img /var/lib/libvirt/images/centos7-samba4.img
[kunohi@centos7-dev-docker ~]$ sudo virsh start centos7-samba4
ドメイン centos7-samba4 が起動されました
[kunohi@centos7-dev-docker ~]$ 
```
VMのクローン
```
sudo virt-install \
  --name centos7-template \
  --hvm \
  --virt-type kvm \
  --ram 1024 \
  --vcpus 1 \
  --arch x86_64 \
  --os-type linux \
  --os-variant rhel7 \
  --network network=default \
  --noautoconsole \
  --disk path=/var/lib/libvirt/images/centos7-template.img,size=10,sparse=true \
  --cdrom /var/lib/libvirt/images/CentOS-7-x86_64-Minimal-1708.iso

sudo virt-clone --original centos7-template --name centos7-test --file /var/lib/libvirt/images/centos7-test.img
sudo virsh start centos7-test
```


centos6のインストール
```
wget http://ftp.riken.jp/Linux/centos/6.9/isos/i386/CentOS-6.9-i386-minimal.iso
wget http://ftp.riken.jp/Linux/centos/6.9/isos/x86_64/CentOS-6.9-x86_64-minimal.iso
sudo mv CentOS-6.9-i386-minimal.iso CentOS-6.9-x86_64-minimal.iso /var/lib/libvirt/images/.

sudo virt-install \
  --name centos6-template \
  --hvm \
  --virt-type kvm \
  --ram 1024 \
  --vcpus 1 \
  --arch x86_64 \
  --os-type linux \
  --os-variant rhel6 \
  --network network=default \
  --noautoconsole \
  --disk path=/var/lib/libvirt/images/centos6-template.img,size=10,sparse=true \
  --cdrom /var/lib/libvirt/images/CentOS-6.9-x86_64-minimal.iso

sudo virt-clone --original centos6-template --name centos6-test --file /var/lib/libvirt/images/centos6-test.img
sudo virsh start centos6-test
```

イメージファイルの移動
```
mkdir /home/images
chown 107.107 /home/images
cp -p /var/lib/libvirt/images/centos6-test.img /home/images/.
cp -p /var/lib/libvirt/images/centos7-samba4.img /home/images/.
cp -p /var/lib/libvirt/images/centos7-test.img /home/images/.
virsh edit centos6-test
+----------------------------------------------------------------------+
    <disk type='file' device='disk'>
      <driver name='qemu' type='qcow2'/>
      <source file='/home/images/centos6-test.img'/>
+----------------------------------------------------------------------+
virsh start centos6-test

virsh edit centos7-test
virsh start centos7-test

virsh edit centos7-samba4
virsh start centos7-samba4

cd /var/lib/libvirt/images
cp -p centos6-template.img /home/images/.
cp -p centos7-template.img /home/images/.
virsh edit centos6-template
virsh edit centos7-template
```

i386のインストール
```
sudo virt-install \
  --name centos6-i386-template \
  --hvm \
  --virt-type kvm \
  --ram 1024 \
  --vcpus 1 \
  --arch i386 \
  --os-type linux \
  --os-variant rhel6 \
  --network network=default \
  --noautoconsole \
  --disk path=/home/images/centos6-i386-template.img,size=10,sparse=true \
  --cdrom /var/lib/libvirt/images/CentOS-6.9-i386-minimal.iso

sudo virsh start centos6-i386-test
```
macアドレスを修正する
```
vi /etc/sysconfig/network-scripts/ifcfg-eth0
   HWADDR
vi /etc/udev/rules.d/70-persistent-net.rules 
/sbin/ifup eth0
```

VMの削除
```
sudo virsh autostart --disable centos6-i386-test
sudo virsh undefine centos6-i386-test
sudo virsh pool-list --all
sudo virsh vol-list images
sudo virsh vol-delete --pool images centos6-i386-test.img
```

VMのIPアドレス一覧
```
sudo virsh net-dhcp-leases default
```

IPアドレスを設定する
```
sudo virsh net-list
sudo virsh net-edit default
grep mac /etc/libvirt/qemu/centos7-samba4.xml
+-------------------------------------------------------------------------+
| <host mac='52:54:00:d5:ab:bc' name='samba-ad' ip='192.168.122.180' />   |
+-------------------------------------------------------------------------+

sudo virsh net-destroy default
sudo rm -rf /var/lib/libvirt/dnsmasq/default.leases
sudo virsh net-start default
cat /var/lib/libvirt/dnsmasq/default.hostsfile
```

ubuntu18.04のインストール
```
wget http://ftp.riken.jp/Linux/ubuntu-releases/18.04/ubuntu-18.04.3-live-server-amd64.iso
sudo virt-install \
  --name ubuntu-18.04 \
  --hvm \
  --virt-type kvm \
  --ram 1024 \
  --vcpus 1 \
  --arch x86_64 \
  --os-type linux \
  --os-variant ubuntu18.04 \
  --network network=default \
  --noautoconsole \
  --disk path=/home/images/ubuntu-18.04.img,size=10,sparse=true \
  --cdrom /var/lib/libvirt/images/ubuntu-18.04.3-live-server-amd64.iso
```
