---
title: "Macで Happy Hacking Keyboardを使う"
date: 2019-12-30T17:53:13+09:00
archives: "2019"
tags: [mac]
author: hidekuno
---

- バックスラッシュを設定する

- Caps Lockキーで英字・日本語入力切替を行う。

![This is a image](/note/img/mac-keyboard.png)

https://www.pfu.fujitsu.com/hhkeyboard/leaflet/hairetu.html