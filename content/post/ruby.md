---
title: "Rubyの導入"
date: 2018-10-12T15:01:59+09:00
archives: "2018"
tags: [ruby]
author: hidekuno
---

rubyのインストール
```
git clone https://github.com/sstephenson/rbenv.git ~/.rbenv

echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bash_profile
echo 'eval "$(rbenv init -)"' >> ~/.bash_profile
exec $SHELL -l
git clone https://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build
rbenv install -v 2.5.1

rbenv rehash
rbenv versions
rbenv global 2.5.1
ruby -v
rbenv exec gem update
```

jekyllのインストール
```
gem install github-pages -v 102
github-pages -v
github-pages versions
jekyll -v
gem install bundler

jekyll new jekyll-demo
jekyll s
```