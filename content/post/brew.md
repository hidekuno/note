---
title: "brewを触ってみる"
date: 2018-10-12T16:03:08+09:00
archives: "2018"
tags: [mac]
author: hidekuno
---
```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew doctor
brew update
brew upgrade
brew install pyenv
echo 'eval "$(pyenv init -)"' >> ~/.bash_profile
exec $SHELL -l
pyenv install -l 
pyenv install anaconda3-5.0.1
pyenv global anaconda3-5.0.1
python --version
conda update conda
```
