---
title: "yum rpm"
date: 2019-02-13T11:10:29+09:00
archives: "2019"
tags: [yum,rpm]
author: hidekuno
---
rpmの依存関係を調べる
```
rpm -qpR postgresql96-server-9.6.10-2PGDG.fsv.x86_64.rpm
```

rpmのスクリプトを調べる
```
rpm -qp --scripts postgresql96-server-9.6.10-2PGDG.fsv.x86_64.rpm
```

ソースファイルをダウンロードする
```
dnf download --source kernel
```

rpmファイルの内容を表示する
```
rpm2cpio  kernel-6.1.21-1.45.amzn2023.src.rpm | cpio --list
```

rpmファイルを展開する
```
rpm2cpio kernel-6.1.21-1.45.amzn2023.src.rpm | cpio -id
```

rpmファイルを展開する(rpmbuildが作成される)
```
rpm -ivh kernel-6.1.21-1.45.amzn2023.src.rpm
```

rpmファイルより、パッチを当てる
```
rpmbuild -bp rpmbuild/SPECS/kernel.spec
```

yumのトランザクション履歴を表示する
```
dnf history python3-libs
```

yumのトランザクション詳細内容を表示する
```
yum history info 12
```