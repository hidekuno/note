---
title: "Ceph"
date: 2023-07-19T13:28:16+09:00
archives: "2023"
tags: [os]
author: hidekuno
---

cephの状態を確認したコマンドの記録
```
pvscan
pvdisplay
vgscan
vgdisplay
lvscan
lvdisplay
mount -t ceph 10.220.1.102,10.220.1.104,10.220.1.106:/ /mnt/pve/cephfs -o name=admin
ceph osd tree
ceph osd status
rbd pool stats
rbd ls
rbd --image vm-100-disk-0 info
ceph-volume lvm list
grep VmSwap /proc/*/status | sort -nr -k 2 | head -10
ps -eo lstart,pid,args|less

ceph df
ceph node ls
ceph config dump
ceph config get osd.0 osd_memory_target
ceph config show osd.0
ceph config show-with-defaults osd.0
```
