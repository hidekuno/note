---
title: "Xmodmap(CtrlとCapsを入れ替える)"
date: 2018-12-19T17:22:03+09:00
archives: "2018"
tags: [x11]
author: hidekuno
---

これで
```
xmodmap -e "remove Lock = Caps_Lock"
xmodmap -e "remove Control = Control_L"
xmodmap -e "keysym Control_L = Caps_Lock"
xmodmap -e "keysym Caps_Lock = Control_L"
xmodmap -e "add Lock = Caps_Lock"
xmodmap -e "add Control = Control_L"
```

