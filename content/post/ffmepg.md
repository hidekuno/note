---
title: "ffmepgで音楽"
date: 2018-10-12T15:52:16+09:00
archives: "2018"
tags: [music]
author: hidekuno
---

mp4からmp3へ変換
```
ffmpeg -i YMO-BGM.mp4 -acodec libmp3lame -ab 128k YMO-BGM.mp3
ffmpeg -i YMO-Technodelic.mp4 -acodec libmp3lame -ab 128k YMO-technodelic.mp3
```

cdから曲を取り出しmp3へ変換
```
cdparanoia -f -B 12
ffmpeg -i track12.cdda.aiff -ab 128k 1963.mp3
mpg123 1963.mp3
```