---
title: "Postfixのあれこれ"
date: 2018-10-12T16:06:06+09:00
archives: "2018"
tags: [mail]
author: hidekuno
---

#### 587でリレーする
```
sudo cat /etc/postfix/relay_password
+------------------------------------------------------------------------
mail.mukogawa.or.jp:587 kuno:xxxxxxxx
+------------------------------------------------------------------------
sudo postmap /etc/postfix/relay_password
sudo systemctl reload postfix.service 
sudo systemctl stop postfix.service 
sudo systemctl start postfix.service 
```

main.cfは下記のように変更
```
113c113
< inet_interfaces = all
---
> #inet_interfaces = all
116c116
< #inet_interfaces = localhost
---
> inet_interfaces = localhost
680,684d679
< 
< relayhost = mail.firstserver.co.jp:587
< smtp_sasl_auth_enable = yes
< smtp_sasl_password_maps = hash:/etc/postfix/relay_password
< smtp_sasl_security_options = noanonymous
```

mailキューの確認
```
postqueue -p
```
mailキューの削除
```
postsuper -d ALL
```

#### 465でリレーする
##### stunnelの導入
```
sudo yum install stunnel
vi /etc/stunnel/smtps.conf
+------------------------------------------------------------------------+
CAfile = /etc/pki/tls/certs/ca-bundle.crt
verify = 2
debug = 6

[smtps]
accept = 11125
client = yes
connect = mail.mukogawa.or.jp
+------------------------------------------------------------------------+
/usr/bin/stunnel /etc/stunnel/smtps.conf
```

##### postfixの設定
```
vi /etc/postfix/relay_password
+------------------------------------------------------------------------+
[127.0.0.1]:11125       kuno:xxxxxxx
+------------------------------------------------------------------------+
postmap /etc/postfix/relay_password
vi /etc/postfix/main.cf
+------------------------------------------------------------------------+
relayhost = [127.0.0.1]:11125
+------------------------------------------------------------------------+
systemctl restart postfix.service
```