---
title: "Python3の導入"
date: 2018-10-12T14:59:48+09:00
archives: "2018"
tags: [python]
author: hidekuno
---

pyenv,anacondaのインストール
```
git clone https://github.com/yyuu/pyenv.git ~/.pyenv
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc
echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(pyenv init -)"' >> ~/.bashrc
source ~/.bashrc

pyenv install -l |grep anaconda
pyenv install anaconda3-5.0.1
pyenv rehash
pyenv global anaconda3-5.0.1
echo 'export PATH="$PYENV_ROOT/versions/anaconda3-5.0.1/bin/:$PATH"' >> ~/.bashrc
source ~/.bashrc
conda update conda
conda install psycopg2
conda update --all
```

pyenv自身の更新
```
git clone https://github.com/pyenv/pyenv-update.git $(pyenv root)/plugins/pyenv-update
pyenv update
```

pip_searchのインストール
```
pip install pip-search
pip_search pandas
```

機械学習用のライブラリのインストール
```
conda install theano
conda install tensorflow
conda install keras
```

condaの環境を一覧表示
```
conda info -e
```

不要なパッケージを削除
```
conda clean --all
```

WSLでpyenvを使った記録
```
git clone https://github.com/yyuu/pyenv.git ~/.pyenv
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc
echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(pyenv init -)"' >> ~/.bashrc
source ~/.bashrc
sudo apt install libncurses-dev libffi-dev libreadline-dev libsqlite3-dev liblzma-dev tk-dev libbz2-dev
pyenv install --list|grep '  3.9.'
pyenv install 3.9.18
pyenv global 3.9.18
```

matplotlibの文字化けを解消する(Mac)
```
cd /tmp
curl -s -O https://ipafont.ipa.go.jp/IPAexfont/ipaexg00301.zip
unzip  ipaexg00301.zip "*.ttf"
cp  ./ipaexg00301/ipaexg.ttf ~/.pyenv/versions/anaconda3-5.0.1/lib/python3.7/site-packages/matplotlib/mpl-data/fonts/ttf/.
rm -f ~/.matplotlib/fontlist-v310.json
rm -f ~/.matplotlib/fontList.json
rm -rf ~/.matplotlib/tex.cache
```
pyソースで以下のように修正する(Mac)
```
font = {'family' : 'IPAexGothic'}
```
グローバルオブジェクト・関数の表示
```
globals()
```
組み込み関数の表示
```
dir(__builtins__)
```
標準ライブラリ(コアライブラリ)の表示
```
help('modules')
```
関数の実行時間を計測する
```
import time

def stopwatch(f):
    s = time.time()
    f()
    r = time.time() - s
    return r
```
