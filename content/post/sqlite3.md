---
title: "sqlite3"
date: 2019-10-18T08:08:14+09:00
archives: "2019"
tags: [db]
author: hidekuno
---

versoinの確認
```
sqlite3 -version
```

DBの接続(なければ作成する)
```
sqlite3 database.cidr
```

SQLの実行 
```
sqlite3 database.cidr  "create table hoge (a char(1));"
```

テーブルの作成
```
CREATE TABLE ipaddr_v4 (
  addr       char(32),
  subnetmask smallint,
  cidr       text,
  country    char(2)
);
```

データのインポート(セミコロンはつけない)
```
.separator \t
.import '/home/kunohi/jvn/tmp/cidr.txt' ipaddr_v4
```

インデックスの作成
```
CREATE INDEX ipaddr_v4_idx1 ON ipaddr_v4(addr);
```

実行計画を表示する
```
explain query plan select count(*) from ipaddr_v4 where addr like '0000000011101%';
```

LIKE検索でINDEXを利用するように設定する
```
PRAGMA case_sensitive_like=ON;
```

queryの処理時間を測定する
```
.timer on
```
